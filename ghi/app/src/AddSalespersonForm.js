import React, {useState} from 'react'

function AddSalespersonForm() {
    const [formData, setFormData] = useState({
      first_name: '',
      last_name: '',
      employee_id: ''
    });
    const [message, setMessage] = useState('');

    const handleChange = (e) => {
      const { name, value } = e.target;
      setFormData({
        ...formData,
        [name]: value
      });
    };

    const handleSubmit = async (e) => {
      e.preventDefault();
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(formData)
        });
        if (response.ok) {
          setMessage('Salesperson added successfully!');
          setFormData({
            first_name: '',
            last_name: '',
            employee_id: ''
          });
        } else {
          const errorData = await response.json();
          setMessage(`Error: ${errorData.error}`);
        }
      } catch (error) {
        console.error('Error:', error);
        setMessage('An error occurred. Please try again.');
      }
    };

    const formFields = [
      { name: 'first_name', label: 'First Name' },
      { name: 'last_name', label: 'Last Name' },
      { name: 'employee_id', label: 'Employee ID' }
    ];

    return (
      <div>
        <h2>Add a Salesperson</h2>
        <form onSubmit={handleSubmit}>
          {formFields.map((field) => (
            <div key={field.name}>
              <label htmlFor={field.name}>{field.label}:</label>
              <input
                type="text"
                id={field.name}
                name={field.name}
                value={formData[field.name]}
                onChange={handleChange}
                required
                autoComplete="off" 
              />
            </div>
          ))}
          <button type="submit">Add Salesperson</button>
        </form>
        {message && <p>{message}</p>}
      </div>
    );
  }

  export default AddSalespersonForm;
