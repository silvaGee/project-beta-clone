import React, { useState } from "react";
function TechnicianForm () {
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        employee_id: "",
    })

    const [message, setMessage] = useState("");
    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
          setMessage ("Successfully added a technician!")
            setFormData({
                first_name: "",
                last_name: "",
                employee_id: "",
            });
        } else{
          setMessage("Employee ID already taken, try again");
        }
    }
    //we're going to replate multiple form change eentListener functions with just this one below
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            //when we do the three dots we are copying over the previous form data into our new state object
            ...formData,
            [inputName]: value //adding currently engaged input key and value
        });

    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                {/* <!-- Now, each field in our form references the same function --> */}
                <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.employee_id} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            {message && <p>{message}</p>}
          </div>
        </div>
      </div>
    )
}
export default TechnicianForm;
