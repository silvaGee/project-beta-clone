import { useEffect, useState } from "react";

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);
  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/technicians/');

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technician)
    }
  };

  useEffect(()=>{
    getData()
  }, [])

  return (
    <>
    <div>
    <h1>Technicians</h1>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee id</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map(technician => (
            <tr key={technician.id}>
              <td>{ technician.first_name }</td>
              <td>{ technician.last_name }</td>
              <td>{ technician.employee_id }</td>
            </tr>
        ))}
      </tbody>
    </table>
    </>
  );
}

export default TechnicianList;
