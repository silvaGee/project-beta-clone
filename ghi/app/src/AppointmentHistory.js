import { useEffect, useState } from "react";


function AppointmentHistory() {
    const [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState('');
    const [search, setSearch] = useState('');

    const getData = async function () {
        const response = await fetch("http://localhost:8080/api/appointments/");
        const autoResponse = await fetch("http://localhost:8100/api/automobiles/");
        if (response.ok && autoResponse.ok) {
            const data = await response.json();
            const autoData = await autoResponse.json();
            const appointmentList = data.appointments;
            const autoList = autoData.automobiles;
            const autoVins = autoList.map(autoData => autoData.vin)
            for (let appointment of appointmentList) {
                if (autoVins.includes(appointment.vin)) {
                    appointment["vip"] = "Yes";
                } else {
                    appointment["vip"] = "No";
                }
            }
            setAppointments(appointmentList);
        };
    };
    useEffect(() => {
      getData()
    },[])

    return (
        <>
            <br></br><h1>Service History</h1><br></br>
            <div className="input-group my-3">
                <input value={vin} onChange={({target}) => setVin(target.value)} placeholder="Search by VIN" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin"></label>
                <button className="btn btn-success" onClick={() => setSearch(vin)}>Search</button>
            </div>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>VIN</th>
                  <th>Is VIP?</th>
                  <th>Customer</th>
                  <th>Date & Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {appointments.filter(appointment => search ? appointment.vin === search : true).map(appointment => {
                  return (
                    <tr key={appointment.id}>
                      <td>{ appointment.vin }</td>
                      <td>{ appointment.vip }</td>
                      <td>{ appointment.customer }</td>
                      <td>{ appointment.date_time }</td>
                      <td>{ appointment.technician }</td>
                      <td>{ appointment.reason }</td>
                      <td>{ appointment.status }</td>
                    </tr>
                  );
                })}
              </tbody>
          </table>
        </>
      );
};
export default AppointmentHistory;
