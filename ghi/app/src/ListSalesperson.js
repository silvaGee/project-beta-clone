import React, { useState, useEffect } from 'react';

function ListSalesPerson() {
  const [salespeople, setSalespeople] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    async function fetchSalespeople() {
      try {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
          const data = await response.json();
          console.log("Fetched Salespeople:", data);
          setSalespeople(data.salesperson || []);
        } else {
          console.error("Failed to fetch salespeople", response.statusText);
          setError("Failed to fetch salespeople: " + response.statusText);
        }
      } catch (error) {
        console.error("Failed to fetch salespeople", error);
        setError("Failed to fetch salespeople: " + error.message);
      }
    }
    fetchSalespeople();
  }, []);

 
  return (
    <div>
      <h1>Salespeople</h1>
      {error && <p>{error}</p>}
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.length === 0 ? (
            <tr>
              <td colSpan="4">No salespeople found</td>
            </tr>
          ) : (
            salespeople.map((salesperson, index) => (
              <tr key={index}>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                <td>{salesperson.employee_id}</td>
                <td>
                </td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
}

export default ListSalesPerson;
