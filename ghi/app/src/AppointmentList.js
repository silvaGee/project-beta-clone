import { useEffect, useState } from "react";

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const autoResponse = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok && autoResponse.ok) {
            const data = await response.json();
            const appointmentList = data.appointments;
            const autoData = await autoResponse.json();
            const autoList = autoData.automobiles;
            const autoVins = autoList.map(autoData => autoData.vin)
            for (let appointment of appointmentList) {
                if (autoVins.includes(appointment.vin)) {
                    appointment["vip"] = "Yes";
                } else {
                    appointment["vip"] = "No";
                }
                setAppointments(appointmentList);
            }
        }
    }
        useEffect(() => {
            getData();
        }, []);

    const cancelAppointment = async function (appointmentToCancel) {
        const response = await fetch (`http://localhost:8080/api/appointments/${appointmentToCancel}/cancel/`, {
            method: "PUT",
        });
        if (response.ok) {
            getData()
        } else {
            console.log(response)
        }
    }

    const finishAppointment = async function (appointmentToFinish) {
        const response = await fetch(`http://localhost:8080/api/appointments/${appointmentToFinish}/finish/`, {
                method: 'PUT',
        })
        if (response.ok) {
            getData()
        } else {
            console.log(response)
        }

    }

  return (
    <>
    <div>
      <h1>Service Appointments</h1>
      </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date & Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
                {appointments.filter(appointment => appointment.status === "created").map(appointment => {
                  return (
                    <tr key={appointment.id}>
                      <td>{ appointment.vin }</td>
                      <td>{ appointment.vip }</td>
                      <td>{ appointment.customer }</td>
                      <td>{ appointment.date_time }</td>
                      <td>{ appointment.technician }</td>
                      <td>{ appointment.reason }</td>
                      <td>
                        <button onClick={() => cancelAppointment(appointment.id)} className="btn btn-danger">Cancel</button>
                        <button onClick={() => finishAppointment(appointment.id)} className="btn btn-success">Finish</button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
    </table>
    </>
  );
}

export default AppointmentList;
