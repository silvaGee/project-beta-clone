import React, { useState, useEffect } from 'react';

function CreateAutomobileForm({ onFormSubmit }) {
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model: '',
  });
  const [message, setMessage] = useState('');
  const [models, setModels] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchModels = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/models/');
        if (!response.ok) {
          throw new Error('Failed to fetch vehicle models');
        }
        const data = await response.json();
        setModels(data.models || []);
      } catch (error) {
        setError('Failed to fetch vehicle models: ' + error.message);
      }
    };

    fetchModels();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const modelId = formData.model;
      const dataToSend = { ...formData, model_id: modelId };
      const response = await fetch('http://localhost:8100/api/automobiles/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(dataToSend),
      });
      const responseData = await response.json();
      if (response.ok) {
        setMessage('Automobile added successfully!');
        setFormData({
          color: '',
          year: '',
          vin: '',
          model: '',
        });
        if (onFormSubmit) {
          onFormSubmit();
        }


      } else {
        setMessage(`Error: ${responseData.message || 'An error occurred'}`);

    }


    } catch (error) {
      console.error('Error:', error);
      setMessage('An error occurred. Please try again.');
    }
  };

  return (
    <div>
      <h2>Create an Automobile in Inventory</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="color">Color:</label>
          <input
            type="text"
            id="color"
            name="color"
            value={formData.color}
            onChange={handleChange}
            required
            autoComplete="off"
          />
        </div>
        <div>
          <label htmlFor="year">Year:</label>
          <input
            type="number"
            id="year"
            name="year"
            value={formData.year}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <label htmlFor="vin">VIN:</label>
          <input
            type="text"
            id="vin"
            name="vin"
            value={formData.vin}
            onChange={handleChange}
            required
            autoComplete="off"
          />
        </div>
        <div>
          <label htmlFor="model">Model:</label>
          <select
            id="model"
            name="model"
            value={formData.model}
            onChange={handleChange}
            required
          >
            <option value="">Select a model</option>
            {models.map((model) => (
              <option key={model.id} value={model.id}>
                {model.name}
              </option>
            ))}
          </select>
        </div>
        <button type="submit">Add Automobile</button>
      </form>
      {message && <p>{message}</p>}
      {error && <p>{error}</p>}
    </div>
  );
}

export default CreateAutomobileForm;
