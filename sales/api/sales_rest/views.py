from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse




class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'sold',
        'id'


    ]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        'first_name',
        'last_name',
        'employee_id',
        'id'


    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'id'

    ]

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        'price',
        'automobile',
        'salesperson',
        'customer',
        'id'

    ]
    encoders= {
        'automobile': AutomobileVODetailEncoder(),
        'salesperson': SalespersonDetailEncoder(),
        'customer': CustomerDetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse({'salesperson': list(salesperson)}, encoder=SalespersonDetailEncoder)
    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            salesperson = Salesperson.objects.create(**data)
            return JsonResponse(salesperson,encoder=SalespersonDetailEncoder,status=201, safe=False)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)


@require_http_methods(["GET", "DELETE"])
def api_salesperson_details(request,pk):
    try:
        salesperson = Salesperson.objects.get(id=pk)
    except Salesperson.DoesNotExist:
        return JsonResponse({'error': "Salesperson does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse(salesperson, encoder=SalespersonDetailEncoder, safe=False)

    elif request.method == "DELETE":
        salesperson.delete()
        return JsonResponse({'message': 'Salesperson deleted successfully'}, status=204)


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": list(customers)}, encoder=CustomerDetailEncoder)

    elif request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(customer, encoder=CustomerDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_customer_deets(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(customer, encoder=CustomerDetailEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": 'Customer does not exist'}, status=404)

    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse({"message": 'Customer deleted successfully'}, status=204)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"}, status=404)



@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({'sales': list(sales)}, encoder=SaleDetailEncoder)

    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            automobile_vin = data["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)

            salesperson_id = data["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            customer_id = data["customer"]
            customer = Customer.objects.get(id=customer_id)

            sale = Sale.objects.create(
                price=data["price"],
                automobile=automobile,
                salesperson=salesperson,
                customer=customer
            )

            automobile.sold = True
            automobile.save()

            response_data = SaleDetailEncoder().encode(sale)
            return JsonResponse(response_data, status=201, safe=False)

        except (ValueError, AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist) as e:
            return JsonResponse({'error': str(e)}, status=400)



@require_http_methods(["DELETE"])
def api_sale_details(request, pk):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse({"message": "Sale deleted successfully"}, status=204)
        except Sale.DoesNotExist:
            return JsonResponse({"error": "Sale does not exist"}, status=404)
