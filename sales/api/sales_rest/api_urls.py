from django.urls import path
from .views import api_list_salesperson,api_salesperson_details, api_list_customer, api_customer_deets, api_list_sales, api_sale_details

urlpatterns = [
    path('salespeople/', api_list_salesperson, name="api_list_salesperson"),
    path('salespeople/<int:pk>/', api_salesperson_details, name="api_salesperson_details"),
    path('customers/', api_list_customer, name="api_list_customer"),
    path('customers/<int:pk>/', api_customer_deets, name="api_customer_deets"),
    path('sales/', api_list_sales, name="api_list_sales"),
    path('sales/<int:pk>/', api_sale_details, name="api_sale_details")

]
