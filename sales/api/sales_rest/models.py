from django.db import models


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)

def __str__(self):
        return self.name

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.TextField()
    phone_number = models.CharField(max_length=20)

def __str__(self):
        return self.name

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    sold = models.BooleanField(default=False)

def __str__(self):
        return self.name

class Sale(models.Model):
    price = models.FloatField()
    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.PROTECT,
        related_name='sales'

    )
    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.PROTECT,
        related_name='sales'
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.PROTECT,
        related_name='sales',


    )

def __str__(self):
        return self.name
